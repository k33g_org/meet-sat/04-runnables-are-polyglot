#!/bin/bash
# Change the GitLab project ID
project_id="33178966"
wasmfile="hello.wasm"
version="0.0.0"
package_name="hello-sat"

SAT_HTTP_PORT=8080 sat https://gitlab.com/api/v4/projects/${project_id}/packages/generic/${package_name}/${version}/${wasmfile}
